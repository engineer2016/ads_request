import openpyxl
from datetime import *

class Demand(object):
    def __init__(self, dt = date(2000, 1, 1)):
        # имя заявителя
        self.name = self.inp_name()
        # адресс заявителя
        self.address = self.inp_address()
        # слесарь выполнявший заявку
        self.mechanik = self.choose_mechanik()
        # вспомагательная перемен. времени поступ. заявки.
        input_time = self.inp_time()
        # разница (до 40 мин) между поступлением и прибытием
        self.time_en_route = self.time_delta()
        # вспомагательная перемен. времени прибытия на заявку
        final_time = self.final_time(input_time, self.time_en_route)
        # дата+время поступления заявки
        self.recive_time = self.comb_date_time(dt, input_time)
        # дата+время прибытия на заявку
        self.running_time = self.comb_date_time(dt, final_time)
        # причина заявки
        self.reason_insident = self.insident()

    def __str__(self):
        return (self.name, self.address, self.recive_time,
                self.running_time, self.time_en_route,
                self.mechanik, self.reason_insident)

    def inp_name(self): #ввод имени абонента
        a = input('Обхідник - нажми "o", ні - enter\n> ')
        if a == 'о':
            name = input("ПІБ абонента > ")
            return name + " (обх)"
        else:
            name = input("ПІБ абонента > ")
            return name

    def inp_address(self): #ввод адреса абонента
        address = input('Введіть адресу абонента.\n> ')
        return address
    def inp_time(self):   # ввід часу виклику
        while True:
            try:
                # ввід часу
                t = time(int(input("годин> ")), int(input("хвилин> ")))
                return t
                break
            except ValueError:
                print("Не допустиме значення, давай заново")
    def time_delta(self): #час в дорозі
        while True:
            t = input("скільки хвилин ви були в дорозі?\n> ")
            if (int(t) > 40):
                print("Більше 40 хвилин. Одумайся!")
            elif (int(t) > 0):
                return int(t)
                break
            else:
                print("Шо то не то, давай ше раз.")

    def insident(self):     #вибір аварійної ситуації
        r1 = ["Витік газу на стояку н.т. ",
            "Витік газу на крані стояка н.т. ",
            "Витік газу на продувній заглушці стояка н.т. "]

        r2 = ["Витік газу на стояку c.т.",
            "Витік газу на крані стояка c.т "]

        r3 = ['Витік газу на атоматиці ОК',
            'Витік газу на крані опуску до ОК',
            'Витік газу на різбовому з’днанні опуску до ОК',
            'Витік газу на гнучкому газопроводі до ОК' ]

        r4 = ['Витік газу на ПГ-4',
            'Витік газу на крані опуску до ПГ-4',
            'Витік газу на різбовому з’днанні опуску до ПГ-4',
            'Витік газу на гнучкому газопроводі до ПГ-4' ]

        r5 = ['Витік газу на ГК',
            'Витік газу на крані опуску до ГК',
            'Витік газу на різбовому з’днанні опуску до ГК',
            'Витік газу на гнучкому газопроводі до ГК']

        r6 = ['Витік газу на конвекторі',
            'Витік газу на крані опуску до конвектора',
            'Витік газу на різбовому з’днанні опуску до конвектора',
            'Витік газу на гнучкому газопроводі до конвектора']

        r7 = ['Витік газу на флянці стояка н.т.',
            'Витік газу на флянці стояка с.т.']

        r8 = ['Витік газу на РДГ',
            'Витік газу на вході в РДГ',
            'Витік газу на виході в РДГ']

        r9 = ['Витік газу на лічильнику',
            'Витік газу на штуцерах лічильника']

        # перелік можливих варантів
        name = ['1 - стояк н.т;', '2 - стояк с.т;', '3 - ОК;', '4 - ПГ;',
                '5 - ГК;', '6 - конвектор;', '7 - флянець;', '8 - РДГ;',
                '9 - лічильник газу', '0 - інша причина']

        # вкладений список
        total = [r1, r2, r3, r4, r5, r6, r7, r8, r9]

        while True:
            try:
                for i in name: print(i)
                a1 = int(input("> "))
                if a1 == 0:
                    b = input("введіть свій варіант\n> ")
                    return b
                    break
                else:
                    j = 1
                    for i in total[a1-1]:       #детальний огляд варіантів
                        print("{0} - {1}".format(j, i))
                        j += 1
                    a2 = int(input("> "))
                    b = (total[a1-1][a2-1])      #рядок з варіантом витоку
                    return b      #рядок з варіантом витоку
                    break
            except (ValueError, IndexError):
                print("Було введно щось невірно, давай знову.")

    def final_time(self, it, dt):  #час прибуття
        try:
            """it-время получения заявки, dt-разница во времени
               rt - время прибытия """
            if (it.minute + dt) > 60:
                h = it.hour + 1
                m = (it.minute + dt)-60
                rt= time(h, m)
                return rt
            else:
                h = it.hour
                m = it.minute + dt
                rt = time(h, m)
                return rt
        except ValueError:
                h = 0
                m = (it.minute + dt)-60
                rt = time(h, m)
                return rt

    def comb_date_time(self, d, t): #совмещает дату у время
        return datetime.combine(d, t)
    def choose_mechanik(self): #выбор слесаря последством выбора местера

        def f(c): #для удобства выбора слесаря
            j = 1
            print("Виберіть слюсаря:")
            for i in c:
                print('{0} - {1}'.format(j, i))
                j += 1
            while True:
                b = input("> ")
                if int(b) in range(1, 17):
                    return c[int(b)-1]
                    break
                else:
                    print("Введіть цифру що відповідає номеру слюсаря")
        """
        if a == 1:
            c = ['Дранішніков Л.О.', 'Тимошенко О.В.',
                 'Гаман А.В.', 'Некрутенко О.М.']
            return f(c)
        elif a == 2:
            c = ['Шерстюк Л.М.', 'Татарченко О.О.',
                 'Партола І.П.', 'Горобець І.М.']
            return f(c)
        elif a == 3:
            c = ['Твердохліб С.А.', 'Коваленко О.О.',
                 'Копилов Ю.Г.', 'Присяженко І.О.']
            return f(c)
        elif a == 4:
            c = ['Стасюк О.А.', 'Кравчук А.М.',
                 'Кучер О.В.', 'Бортяний П.А.']
            return f(c)
        """
        c = ['Дранішніков Л.О.', 'Тимошенко О.В.',
                'Гаман А.В.', 'Некрутенко О.М.',
                'Шерстюк Л.М.', 'Татарченко О.О.',
                'Партола І.П.', 'Горобець І.М.',
                'Твердохліб С.А.', 'Коваленко О.О.',
                'Копилов Ю.Г.', 'Присяженко І.О.',
                'Стасюк О.А.', 'Кравчук А.М.',
                'Кучер О.В.', 'Бортяний П.А.']

        return f(c)


def choose_master():
    while True:
        try:
            master = int(input("""Виберіть майстра:
                                \r1 - Антоненко Т.В.,
                                \r2 - Білий О.В.,
                                \r3 - Загаєцький А.А.,
                                \r4 - Мартиненко В.О.\n> """))
            if master in range(1, 5):
                return master
                break
        except ValueError:
            print('Не тупи! : 1 це Таня, 2 це Олег, 3 це Андрій. 4 це Віталік')

def date_():     # ввід дати
    while True:
        print("Введіть дату")
        try:
            d = date(
                int(input("рік> ")),
                int(input("місяць> ")),
                int(input("день> "))
                )
            return d
            break
        except ValueError:
            print("Не вірна дата! Пробуй ще")

def beauty_datetime(d): #делает красивенькую дату для вставки в эксель
    return d.strftime('%d.%m.%Y %H:%M')

def beauty_date(d:'date_in_standart_form')->str: #делает красивенькую дату для вставки в эксель
    return d.strftime('%d.%m.%Y')

def some_func(a:'str_variable')->'1st case date, 2nd stop cycle':
    if a == 'з':
        a = input("змінити дату - 'д', завершити - 'к'.\n> ")
        if a == 'д':
            dt = date_()
            return dt
        elif a == 'к':
            # place for saving options
            print("Завершення программи")
            return False
        else:
            print("змінити дату, або закінчити!!!! а не те шо ти...")

def get_inf(ld:'list with demand object')->'xlsx. file':
    ll = []
    for i in ld:
        ll.append(i.name)
        ll.append(i.address)
        ll.append(beauty_datetime(i.recive_time))
        ll.append(beauty_datetime(i.running_time))
        ll.append(i.time_en_route)
        ll.append(i.mechanik)
        ll.append(i.reason_insident)
        ws.append(ll)
        ll = []

dt = date_()
l = []

while True:
    dm = Demand(dt)
    l.append(dm)
    wb = openpyxl.Workbook()
    ws = wb.active
    ws.title = 'список'
    get_inf(l)
    wb.save('autosave.xlsx')
    a = input("датa/завершення-'з'.\n> ")
    b = some_func(a)
    if b == None:
        pass
    elif b == False:
        print("Завершення программи")
        break
    else:
        dt = b

g = lambda x: x.__str__()

wb = openpyxl.Workbook()
ws = wb.active

ws.title = 'список'
get_inf(l)
wb.save(input('input file name\n> ') + '.xlsx')

